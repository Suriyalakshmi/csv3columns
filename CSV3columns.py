import datetime
import calendar
import pandas as pd
from datetime import date, timedelta

month=int(input('enter the month :'))
K=int(input("ENTER HOW MANY ROWS :"))
Contact= ["De Gruyter Poland Sp z oo"]

def InvoiceDatefn(month):
  currentDateTime = datetime.datetime.now()
  date = currentDateTime.date()
  year = date.strftime("%Y")
  monthrange=calendar.monthrange(int(year),month)
  Firstdate,Lastdate=monthrange
  if(month<10):
     Invoice="{}-0{}-{}".format(Lastdate,month,int(year))
  else:
     Invoice="{}-{}-{}".format(Lastdate,month,int(year))
  return(Invoice)
  
def DueDatefn(ID):
  date = pd.to_datetime(ID)
  days_after = (date+timedelta(days=30))
  return days_after


def Loop(month,K,Contact):
  ID=InvoiceDatefn(month)
  Contact=Contact*K
  InvoiceDate=[ID]*K
  DueDate=[DueDatefn(ID)]*K
  dict1={"ContactName" : Contact,"InvoiceDate" : InvoiceDate, "DueDate" : DueDate}
  df2=pd.DataFrame(dict1)
  return df2
df=Loop(month,K,Contact)
df.to_csv("Example.csv",index=False)
df